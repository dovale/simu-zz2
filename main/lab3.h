#ifndef LAB3_H
#define LAB3_H

#include <stdio.h>
#include <stdlib.h>

double simPi(int nb_points);
double* replicatesPI (int n, int nb_points);
double meanPI(int n, int nb_points);

double Sigma2Variiance(int n, int nb_points, double mean);
double margin_of_error(int n, int nb_points); 

#endif
