#ifndef LAB2_H
#define LAB2_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <tgmath.h>

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static unsigned long mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

void init_genrand(unsigned long s);
void init_by_array(unsigned long init_key[], int key_length);
unsigned long genrand_int32(void);

double uniform(double a, double b);

double* discrete3(double a, double b, double c, int iter);
double* discrete(int size, double* obs);
double* new_observations_simulation(int drawings, int size, double* cdf);

double negExp(double Mean);
double mean_negExp(int n, double Mean);
int* ArrayTest_negExp(int drawings, double Mean);

int Dice_Simulation(int faces);
int* Dice_Experiment(int n);
int* Test_Box_Muller(int drawings);

#endif