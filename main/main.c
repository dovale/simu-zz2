#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <tgmath.h>
#include "lab2.h"
#include "lab3.h"

int main(void)
{
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    // lab2
/*

// Generation of uniform random numbers between A and B
    printf("Random temperatures between -98°C and 57,7°C :\n");

    for (int i=0;i<100;i++){
        printf("%10.1f  ",uniform(-98,57.7));
    }

    printf("\n");

// Reproduction of discrete empirical distributions
    init_by_array(init, length);
    printf("\n");

    printf("Discrete empirical distributions with 3 classes :\n");

    printf("1 000 drawings :\n");
    printf("A : %10.1f , B : %10.1f , C : %10.1f\n", discrete3(35,45,20,1000)[0]/10,discrete3(35,45,20,1000)[1]/10,discrete3(35,45,20,1000)[2]/10);
    printf("10 000 drawings :\n");
    printf("A : %10.1f , B : %10.1f , C : %10.1f\n", discrete3(35,45,20,10000)[0]/10,discrete3(35,45,20,10000)[1]/10,discrete3(35,45,20,10000)[2]/10);
    printf("100 000 drawings :\n");
    printf("A : %10.1f , B : %10.1f , C : %10.1f\n", discrete3(35,45,20,100000)[0]/10,discrete3(35,45,20,100000)[1]/10,discrete3(35,45,20,100000)[2]/10);
    printf("1 000 000 drawings :\n");
    printf("A : %10.1f , B : %10.1f , C : %10.1f\n", discrete3(35,45,20,1000000)[0]/10,discrete3(35,45,20,1000000)[1]/10,discrete3(35,45,20,1000000)[2]/10);

    printf("\n");

    printf("Discrete empirical distributions with 6 classes :\n");

    int size=6;
    double obs[]={100, 400, 600, 400, 100, 200};

    double* cdf=discrete(size,obs);
    double* res=new_observations_simulation(1000000, size, cdf);

    printf("Cumulative Density Function :\n");
    for(int i=0;i<size;i++){
        printf("%d : %10.3f\n", i+1, cdf[i]);
    }

    printf("\n");

    printf("New Observations (1 000 000 drawings):\n");
    for(int i=0;i<size;i++){
        printf("%d : %10.3f\n", i+1, res[i]);
    }

    printf("\n");

// Reproduction of continuous distributions
    printf("negExp with a mean of 10\n");

    printf("\n");

    printf("1 000 drawings : \n");

    printf("\n");
    init_by_array(init, length);

    printf("Mean : %10.3f\n", mean_negExp(1000,10));

    printf("\n");
    init_by_array(init, length);

    int* TestBins1=ArrayTest_negExp(1000,10);

    printf("Frequency : \n");
    for(int i=0; i<20; i++)printf("number between %d and %d : %10.3d\n", i, i+1, TestBins1[i]);
    printf("number above 20 : %10.3d\n", TestBins1[20]);

    printf("\n");
    init_by_array(init, length);
    
    printf("Histogram :\n");
    for (int i=0; i<20; i++){
        for (int j=0; j<(int)TestBins1[i]; j++){
            printf("*");
        }
        printf("\n");
    }

    printf("\n");


    printf("1 000 000 drawings : \n");

    printf("\n");
    init_by_array(init, length);

    printf("Mean : %10.3f\n", mean_negExp(1000000,10));

    printf("\n");
    init_by_array(init, length);
    
    int* TestBins2=ArrayTest_negExp(1000000,10);

    printf("Frequency : \n");
    for(int i=0; i<20; i++)printf("number between %d and %d : %10.3d\n", i, i+1, TestBins2[i]);
    printf("number above 20 : %10.3d\n", TestBins2[20]);

    printf("\n");
    init_by_array(init, length);
    
    printf("Histogram :\n");
    for (int i=0; i<20; i++){
        for (int j=0; j<(int)TestBins2[i]/1000; j++){
            printf("*");
        }
        printf("\n");
    }

    printf("\n");

// Simulating non reversible distribution laws
    init_by_array(init, length);

    printf("Dice Experiment :\n");

    int* Bins=Dice_Experiment(1000000);

    for (int j=0; j<201; j++){
        printf("%d  ", Bins[j]);
    }

    printf("\n\n");

    printf("Histogram :\n");
    for (int i=0; i<201; i++){
        for (int j=0; j<Bins[i]/1000; j++){
            printf("*");
        }
        printf("    %d\n",i+40);
    }

    printf("\n");

    init_by_array(init, length);

    printf("Test of the Box and Muller functions :\n");

    int* Bins_BM=Test_Box_Muller(1000000);
    for (int j=0; j<21; j++){
        printf("%d  ",Bins_BM[j]);
    }

    printf("\n");

    printf("Histogram :\n");
    for (int i=0; i<21; i++){
        for (int j=0; j<Bins_BM[i]/10000; j++){
            printf("*");
        }
        printf("    %1.1f\n",(double)i/2-5);
    }



*/

    // lab3

    double pi=simPi(1000);
    printf("pi = %10.5f\n", pi);
    pi=simPi(1000000);
    printf("pi = %10.5f\n", pi);
    // pi=simPi(1000000000);
    // printf("pi = %10.5f\n", pi);

    pi=meanPI(10,1000000);
    printf("pi = %10.7f\n", pi);
    //pi=meanPI(40,1000000000);
    //printf("pi = %10.7f\n", pi);

    printf("absolute_error = %10.7f\n", M_PI-pi);
    printf("relative_error = %10.7f\n", pi/M_PI); 

    double margin = margin_of_error(10, 1000000); 

    // Affichage des résultats
    printf("Moyenne de pi : %10.7f\n", pi);
    printf("Margin : %10.7f\n", margin);
    printf("Intervalle de confiance à 95%% : [%10.7f, %10.7f]\n", pi - margin, pi + margin);


    pi=meanPI(40,1000000); 
    margin = margin_of_error(40, 1000000); 

    // Affichage des résultats
    printf("Moyenne de pi : %10.7f\n", pi);
    printf("Margin : %10.7f\n", margin);
    printf("Intervalle de confiance à 95%% : [%10.7f, %10.7f]\n", pi - margin, pi + margin);
    
    return 0;
}
