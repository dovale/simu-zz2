/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)  
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <tgmath.h>
#include "lab2.h"

/* initializes mt[N] with a seed */
void init_genrand(unsigned long s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] = 
	    (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array(unsigned long init_key[], int key_length)
{
    int i, j, k;
    init_genrand(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32(void)
{
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }
  
    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}


//
double uniform(double a, double b){
    return a+(b-a)*genrand_int32()*(1.0/4294967295.0);
}

double* discrete3(double a, double b, double c, int iter){
    double*res=malloc(3*sizeof(double));
    for(int i=0;i<3;i++){
        res[i]=0;
    }
    int p=0;
    for(int i=0; i<iter; i++) {
        p=uniform(0,100);
        if (p<a){
            res[0]+=1;
        }
        else if(p<a+b){
            res[1]+=1;
        }
        else{
            res[2]+=1;
        }
    }
    return res;
}

double* discrete(int size, double* obs){
    double total=0;
    double* cdf=malloc(size*sizeof(double));
    for(int i=0; i<size; i++){
        total+=obs[i];
        cdf[i]=total;
    }
    for(int i=0;i<size;i++){
        cdf[i]=cdf[i]/total;
    }
    return cdf;
}

double* new_observations_simulation(int drawings, int size, double* cdf){
    double*res=malloc(size*sizeof(double));
    for (int i=0;i<size;i++){
        res[i]=0;
    }
    double p=0;

    for(int i=0;i<drawings;i++){
        p=uniform(0,1);
        for (int j=0;j<size;j++){
            if(p<cdf[j]){
                res[j]+=1;
                j=size;
            }
        }
    }

    return res;
}

double negExp(double Mean){
    return -Mean* log(1-uniform(0,1));
}

double mean_negExp(int n, double Mean){
    double mean=0;
    for(int i=0; i<n; i++){
        mean += negExp(Mean);
    }
    return mean/n;
}

int* ArrayTest_negExp(int drawings, double Mean){
    int* Test21bins=malloc(21*sizeof(int));
    int test=0;
    for(int i=0; i<21; i++)Test21bins[i]=0;
    for(int i=0; i<drawings; i++){
        test=(int)negExp(Mean);
        if(test<20){
            Test21bins[test]++;
        }
        else{
            Test21bins[20]++;
        }
    }
    return Test21bins;
}

int Dice_Simulation(int faces){
    return (int)uniform(1,faces);
}

int* Dice_Experiment(int n){
    int x = 0;
    int* Bins = malloc(201*sizeof(int));

    for (int j=0; j<201; j++){
        Bins[j]=0;
    }

    for (int j=0; j<n; j++){
        for (int i=0; i<40; i++){
            x+=Dice_Simulation(6);
        }
        Bins[x-40]+=1;
        x=0;
    }

    return Bins;
}

int* Test_Box_Muller(int drawings){
    int* Bins=malloc(21*sizeof(int));
    for (int j=0; j<21; j++){
        Bins[j]=0;
    }
    double Rn1=0;
    double Rn2=0;
    double x1=0;
    double x2=0;
    for(int i=0; i<drawings; i++){
        Rn1=uniform(0,1);
        Rn2=uniform(0,1);
        x1=cos(2*M_PI*Rn2)*sqrt((-2*log(Rn1)));
        x2=sin(2*M_PI*Rn2)*sqrt((-2*log(Rn1)));
        Bins[(int)(10+x1*2)]+=1;
        Bins[(int)(10+x2*2)]+=1;
    }
    return Bins;
}