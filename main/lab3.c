#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "lab2.h"
#include "lab3.h"

double simPi(int nb_points){
    double x;
    double y;
    double in=0;
    for (int i=0; i<nb_points; i++){
        x=uniform(-1,1);
        y=uniform(-1,1);
        // printf("x = %10.4f", x);
        // printf("y = %10.4f", y);
        // printf("\n");
        if (x*x+y*y<1){
            in+=1;
        }
        // printf("in = %10.4f", in);
    }
    return 4*in/nb_points;
}

double* replicatesPI (int n, int nb_points){
    double* replicates = malloc(n*sizeof(double));
    for(int i=0; i<n; i++){
        replicates[i] = simPi(nb_points);
    }
    return replicates;
}

double meanPI(int n, int nb_points){
    double mean=0;
    for(int i=0; i<n; i++){
        mean += simPi(nb_points);
    }
    return mean/n;
}

double Sigma2Variance(int n, int nb_points, double mean) {
    double sum = 0;
    double pi = 0;
    for (int i = 0; i < n; i++) {
        pi = simPi(nb_points);
        sum += (pi - mean) * (pi - mean);
    }
    return sum / (n - 1);
}

double margin_of_error(int n, int nb_points) {
    return (1.96 * sqrt(Sigma2Variance(n, nb_points, meanPI(n, nb_points)) / n));
}

